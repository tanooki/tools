#!/bin/bash

PROJECT_ID=25707498
PIPELINE_ID=304461629
curl --request DELETE \
     --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" \
     --header "content-type: application/json" \
     --url "https://gitlab.com/api/v4/projects/${PROJECT_ID}/pipelines/${PIPELINE_ID}" 

# Ref cf https://stackoverflow.com/questions/53355578/how-to-delete-gitlab-ci-jobs-pipelines-logs-builds-and-history

